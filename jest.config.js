module.exports = {
  moduleNameMapper: {
    '^~(/.*)$': '<rootDir>/src$1',
  },
  setupFilesAfterEnv: ["jest-dom/extend-expect"],
  testMatch: ["<rootDir>/test/**/*.test.js"],
  verbose: true,
};
