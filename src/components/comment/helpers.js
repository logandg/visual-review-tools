import { nextView } from '../../store/view';
import { localStorage, COMMENT_BOX, STORAGE_MR_ID, STORAGE_TOKEN } from '../../shared';
import { clearNote } from '../note';
import { addForm } from '../wrapper/helpers';

const clearMrId = state => {
  localStorage.removeItem(STORAGE_MR_ID);
  state.mergeRequestId = '';
};

const changeSelectedMr = state => {
  clearMrId(state);
  clearNote();
  addForm(nextView(state, COMMENT_BOX));
};

// This function is here becaause it is called only from the comment view
// If we reach a design where we can logout from multiple views, promote this
// to it's own package
const logoutUser = state => {
  localStorage.removeItem(STORAGE_TOKEN);
  localStorage.removeItem(STORAGE_MR_ID);
  state.token = '';
  state.mergeRequestId = '';

  clearNote();
  addForm(nextView(state, COMMENT_BOX));
};

export { changeSelectedMr, clearMrId, logoutUser };
