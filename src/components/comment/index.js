import { COMMENT_BOX, LOGOUT } from '../../shared';
import { buttonClearStyles } from '../utils';
import selectedMrNote from './mr_note';
import { getSavedComment } from './storage';

const comment = state => {
  const savedComment = getSavedComment();

  return `
    <div>
      <textarea
        id="${COMMENT_BOX}"
        name="${COMMENT_BOX}"
        rows="3"
        placeholder="Enter your feedback or idea"
        class="gl-bkg-white gl-w-100p gl-border-gray-200 gl-border-solid gl-border-w-1 gl-border-r-1 gl-min-h-5 gl-py-05 gl-px-1"
        aria-required="true"
      >${savedComment}</textarea>
      <div class="gl-text-small gl-gray-700 gl-mb-2">
        ${selectedMrNote(state)}
        <p class="gl-mb-2">
          Additional metadata will be included: browser, OS, current page, user agent, and viewport dimensions.
        </p>
      </div>
    </div>
    <div class="js-gitlab-button-wrapper gl-flex gl-flex-row-reverse gl-align-baseline gl-mt-2">
      <button class="gitlab-button gitlab-button-success" style="${buttonClearStyles}" type="button" id="gitlab-comment-button"> Send feedback </button>
      <button class="gitlab-button gitlab-button-secondary" style="${buttonClearStyles}" type="button" id="${LOGOUT}"> Log out </button>
    </div>
  `;
};

export default comment;
