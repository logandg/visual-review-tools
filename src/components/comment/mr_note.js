import { CHANGE_MR_ID_BUTTON } from '../../shared';
import { buttonClearStyles } from '../utils';

const selectedMrNote = state => {
  const { mrUrl, projectPath, mergeRequestId } = state;

  const mrLink = `${mrUrl}/${projectPath}/merge_requests/${mergeRequestId}`;

  return `
    <p class="gl-mb-2">
      This posts to merge request <a class="gl-blue-600 gl-no-underline gl-underline-hover" href="${mrLink}">!${mergeRequestId}</a>.
      <button style="${buttonClearStyles}" type="button" id="${CHANGE_MR_ID_BUTTON}" class="gl-blue-600 gl-no-underline gl-underline-hover gitlab-link-button">Change</button>
    </p>
  `;
};

export default selectedMrNote;
