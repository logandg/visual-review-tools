import { REMEMBER_ITEM } from '../shared';
import { buttonClearStyles } from './utils';

const rememberBox = (rememberText = 'Remember me') => `
  <div class="gl-flex gl-align-baseline">
    <input type="checkbox" id="${REMEMBER_ITEM}" name="${REMEMBER_ITEM}" value="remember">
    <label for="${REMEMBER_ITEM}" class="gl-p-1">${rememberText}</label>
  </div>
`;

const submitButton = buttonId => `
  <div class="js-gitlab-button-wrapper gl-flex gl-align-baseline gl-mt-2">
    <button class="gitlab-button gitlab-button-success gl-w-100p" style="${buttonClearStyles}" type="submit" id="${buttonId}"> Submit </button>
  </div>
`;
export { rememberBox, submitButton };
