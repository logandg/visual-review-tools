import { nextView } from '../../store/view';
import { localStorage, LOGIN, TOKEN_BOX, STORAGE_TOKEN } from '../../shared';
import { clearNote, postError } from '../note';
import { selectRemember, selectToken } from '../utils';
import { addForm } from '../wrapper/helpers';

const storeToken = (token, state) => {
  const rememberMe = selectRemember().checked;

  if (rememberMe) {
    localStorage.setItem(STORAGE_TOKEN, token);
  }

  state.token = token;
};

const authorizeUser = state => {
  // Clear any old errors
  clearNote(TOKEN_BOX);

  const token = selectToken().value;

  if (!token) {
    postError('Please enter your token.', TOKEN_BOX);
    return;
  }

  storeToken(token, state);
  addForm(nextView(state, LOGIN));
};

export { authorizeUser, storeToken };
