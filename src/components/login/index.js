import { LOGIN, TOKEN_BOX } from '../../shared';
import { rememberBox, submitButton } from '../form_elements';

const labelText = `
  Enter your
    <a
      class="gl-blue-600 gl-no-underline gl-underline-hover"
      href="https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html"
    >
      personal access token
    </a>
`;

const login = `
    <div>
      <label for="${TOKEN_BOX}" class="gl-inline-block gl-text-heavy gl-w-100p gl-mb-1">${labelText}</label>
      <input class="gl-w-100p gl-border-gray-200 gl-border-solid gl-border-w-1 gl-border-r-1 gl-py-05 gl-px-1 gl-min-h-5" type="password" id="${TOKEN_BOX}" name="${TOKEN_BOX}" autocomplete="current-password" aria-required="true">
    </div>
    ${rememberBox()}
    ${submitButton(LOGIN)}
`;

export default login;
