import { MR_ID, MR_ID_BUTTON } from '../../shared';
import { rememberBox, submitButton } from '../form_elements';

const mrLabel = `Enter your merge request ID`;
const mrRememberText = `Remember this number`;

const mrForm = `
    <div>
      <label for="${MR_ID}" class="gl-inline-block gl-text-heavy gl-w-100p gl-mb-1">${mrLabel}</label>
      <input class="gl-w-100p gl-border-gray-200 gl-border-solid gl-border-w-1 gl-border-r-1 gl-py-05 gl-px-1 gl-min-h-5" type="text" pattern="[1-9][0-9]*" id="${MR_ID}" name="${MR_ID}" placeholder="e.g., 321" aria-required="true">
    </div>
    ${rememberBox(mrRememberText)}
    ${submitButton(MR_ID_BUTTON)}
`;

export default mrForm;
