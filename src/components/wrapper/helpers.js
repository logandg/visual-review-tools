import { CLEAR, WHITE } from '../../shared';
import {
  selectCollapseButton,
  selectForm,
  selectFormContainer,
  selectNoteContainer,
} from '../utils';
import { commentIcon, compressIcon } from './icons';

const addForm = nextForm => {
  const formWrapper = selectForm();
  formWrapper.innerHTML = nextForm;
};

function toggleForm() {
  const toggleButton = selectCollapseButton();
  const currentForm = selectForm();
  const formContainer = selectFormContainer();
  const noteContainer = selectNoteContainer();
  const OPEN = 'open';
  const CLOSED = 'closed';

  const openButtonClasses = ['js-gitlab-collapse-open'];
  const closedButtonClasses = ['gl-align-self-center'];
  const openContainerClasses = ['gl-max-h-max', 'gl-max-w-max', 'gl-p-3', 'gl-bkg-white'];
  const openFormClasses = ['gl-flex'];
  const closedFormClasses = ['gl-display-none'];

  const containerClasses = [...openContainerClasses];
  const buttonClasses = [...openButtonClasses, ...closedButtonClasses];
  const formClasses = [...openFormClasses, ...closedFormClasses];

  const toggleAll = (node, classArr) => {
    classArr.forEach(classItem => node.classList.toggle(classItem));
  };

  const stateVals = {
    [OPEN]: {
      icon: compressIcon,
      display: 'flex',
      backgroundColor: WHITE,
    },
    [CLOSED]: {
      icon: commentIcon,
      display: 'none',
      backgroundColor: CLEAR,
    },
  };

  const nextState = toggleButton.classList.contains('js-gitlab-collapse-open') ? CLOSED : OPEN;
  const currentVals = stateVals[nextState];

  toggleAll(formContainer, containerClasses);
  formContainer.style.backgroundColor = currentVals.backgroundColor;
  toggleAll(currentForm, formClasses);
  toggleAll(toggleButton, buttonClasses);
  toggleButton.innerHTML = currentVals.icon;

  if (noteContainer && noteContainer.innerText.length > 0) {
    noteContainer.style.display = currentVals.display;
  }
}

export { addForm, toggleForm };
