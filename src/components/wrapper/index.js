import { FORM, FORM_CONTAINER } from '../../shared';
import { collapseButton } from './icons';

const form = content => `
  <form id="${FORM}" class="gl-flex gl-flex-column gl-w-100p gl-mb-0" novalidate>
    ${content}
  </form>
`;

const buttonAndForm = content => `
  <div id="${FORM_CONTAINER}" class="gl-p-3 gl-bkg-white gl-flex gl-border-r-1 gl-flex-row-reverse gl-overflow-auto">
    ${collapseButton}
    ${form(content)}
  </div>
`;

export default buttonAndForm;
