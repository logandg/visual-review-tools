import './styles/buttons.css';
import './styles/toolbar.css';
import './styles/utilities.css';

import { note, selectForm, selectContainer, wrapper } from './components';
import { REVIEW_CONTAINER } from './shared';
import { eventLookup, getInitialView, initializeGlobalListeners, initializeState } from './store';

/*

  Welcome to the visual review toolbar files. A few useful notes:

  - These files build a static script that is served from our webpack
    assets folder via the npm package. (https://gitlab.com/assets/webpack/visual_review_toolbar.js)

  - To compile this file, run `yarn webpack`. To compile and deploy the dev version, run `yarn dev-deploy`.

  - Vue is not used in these files because we do not want to ask users to
    install another library at this time. It's all pure vanilla javascript.

*/

const createContainer = (document, state) => {
  const containerClasses = `
    gl-text-base
    gl-gray-900
    gl-max-h-max
    gl-max-w-max
    gl-overflow-auto
    gl-z-top
    gl-fixed
    gl-bottom-3
    gl-right-3
    gl-flex
    gl-flex-column
  `;

  const mainContent = wrapper(getInitialView(state));
  const container = document.createElement('div');
  container.setAttribute('id', REVIEW_CONTAINER);
  container.setAttribute('class', containerClasses);
  container.insertAdjacentHTML('beforeend', note);
  container.insertAdjacentHTML('beforeend', mainContent);

  document.body.insertBefore(container, document.body.firstChild);
};

window.addEventListener('load', () => {
  const state = initializeState(window, document);
  createContainer(document, state);

  selectContainer().addEventListener('click', event => {
    eventLookup(event.target.id)();
  });

  selectForm().addEventListener('submit', event => {
    // this is important to prevent the form from adding data
    // as URL params and inadvertently revealing secrets
    event.preventDefault();

    const { id } = event.target.querySelect('.js-gitlab-button-wrapper button[type=submit]') || {};

    // even if this is called with false, it's ok; it will get the default no-op
    eventLookup(id)();
  });

  initializeGlobalListeners();
});
