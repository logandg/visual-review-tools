import { hasStorageSupport } from '../shared';

const state = {
  browser: '',
  usingGracefulStorage: '',
  innerWidth: '',
  innerHeight: '',
  mergeRequestId: '',
  mrUrl: '',
  platform: '',
  projectId: '',
  userAgent: '',
  token: '',
};

// adapted from https://developer.mozilla.org/en-US/docs/Web/API/Window/navigator#Example_2_Browser_detect_and_return_an_index
const getBrowserId = sUsrAg => {
  const aKeys = ['MSIE', 'Edge', 'Firefox', 'Safari', 'Chrome', 'Opera'];
  let nIdx = aKeys.length - 1;

  for (nIdx; nIdx > -1 && sUsrAg.indexOf(aKeys[nIdx]) === -1; nIdx -= 1);
  return aKeys[nIdx];
};

const initializeState = (wind, doc) => {
  const {
    innerWidth,
    innerHeight,
    navigator: { platform, userAgent },
  } = wind;

  const browser = getBrowserId(userAgent);

  // It only matters that we are using gracefulStorage for the sessionStorage
  // because it is related to unsubmitted comments
  const usingGracefulStorage = !hasStorageSupport(wind.sessionStorage);

  const scriptEl = doc.getElementById('review-app-toolbar-script');
  const { projectId, mergeRequestId, mrUrl, projectPath } = scriptEl.dataset;

  // This mutates our default state object above. It's weird but it makes the linter happy.
  Object.assign(state, {
    browser,
    innerWidth,
    innerHeight,
    mergeRequestId,
    mrUrl,
    platform,
    projectId,
    projectPath,
    userAgent,
    usingGracefulStorage,
  });

  return state;
};

export { initializeState, state };
