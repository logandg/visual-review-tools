import { comment, login, mrForm } from '../components';
import { localStorage, COMMENT_BOX, LOGIN, MR_ID, STORAGE_MR_ID, STORAGE_TOKEN } from '../shared';

const nextView = (appState, form = 'none') => {
  const formsList = {
    [COMMENT_BOX]: currentState => (currentState.token ? mrForm : login),
    [LOGIN]: currentState => (currentState.mergeRequestId ? comment(currentState) : mrForm),
    [MR_ID]: currentState => (currentState.token ? comment(currentState) : login),
    none: currentState => {
      if (!currentState.token) {
        return login;
      }

      if (currentState.token && !currentState.mergeRequestId) {
        return mrForm;
      }

      return comment(currentState);
    },
  };

  return formsList[form](appState);
};

const getInitialView = state => {
  const token = localStorage.getItem(STORAGE_TOKEN);
  const mrId = localStorage.getItem(STORAGE_MR_ID);

  if (token) {
    state.token = token;
  }

  if (mrId) {
    state.mergeRequestId = mrId;
  }

  return nextView(state);
};

export { getInitialView, nextView };
