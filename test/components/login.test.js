import { LOGIN, RED } from '~/shared';
import login from '~/components/login';
import { storeToken, authorizeUser } from '~/components/login/helpers';
import { selectMrBox, selectNote, selectToken } from '~/components/utils';
import { createWrapperWithContent, setHTMLFixture } from '../helpers';

const selectLoginButton = () => document.getElementById(LOGIN);
const addToken = tok => {
  selectToken().value = tok;
};

const state = {};
const token = 'ghe7653-';

describe('the login component', () => {
  beforeEach(() => {
    setHTMLFixture(login);
  });

  describe('the view', () => {
    it('contains the login field', () => {
      expect(selectToken()).toBeInTheDocument();
    });

    it('encodes the field as a password type', () => {
      expect(selectToken()).toHaveAttribute('type', 'password');
    });

    it('adds the password autocomplete', () => {
      expect(selectToken()).toHaveAttribute('autocomplete', 'current-password');
    });

    it('contains the login button', () => {
      expect(selectLoginButton()).toBeInTheDocument();
    });

    it('matches the view snapshot', () => {
      expect(document.body).toMatchSnapshot();
    });
  });

  describe('store token', () => {
    it('adds the token to the state', () => {
      storeToken(token, state);
      expect(state).toMatchObject({ token });
    });
  });

  describe('authorize user', () => {
    beforeEach(() => {
      createWrapperWithContent(login);
    });

    it('returns an error when there is no token value', () => {
      authorizeUser(state);
      expect(selectNote().innerText).toBe('Please enter your token.');
      expect(selectToken()).toHaveStyle(`borderColor: ${RED}`);
    });

    it('does not set state when there is no token value', () => {
      authorizeUser(state);
      expect(state).toMatchObject({});
    });

    it('sets state when there is a token value', () => {
      addToken(token);
      authorizeUser(state);
      expect(state).toMatchObject({ token });
    });

    it('moves to next view when there is a token value', () => {
      addToken(token);
      authorizeUser(state);
      expect(selectMrBox()).toBeInTheDocument();
    });
  });
});
